from django.db import models

class Testimony(models.Model):
    #Nome da testemunha
    author_name = models.CharField("name", max_length=150, blank=True, null=True)
    #Flag se ela quer ser identificada ou nao
    anonymous = models.NullBooleanField(help_text='Select this option to remain anonymous')
    #Data de envio do depoimento
    date = models.DateTimeField()
    #Depoimento
    testimony = models.TextField()
    #Flag para mostrar se o depoimento esta aprovado para publicacao
    approved = models.BooleanField()
    def __unicode__(self):
        return u"%s %s" % (self.date, self.author_name)
