from testimonies.models import Testimony
from django.contrib import admin

class TestimonyAdmin(admin.ModelAdmin):
    list_display = ('date', 'testimony', 'approved')
    list_filter = ['approved']
    search_fields = ['testimony']
    list_editable = ['approved']
    ordering = ['-date']
admin.site.register(Testimony, TestimonyAdmin)
