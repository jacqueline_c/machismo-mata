from django.db import models

class Death(models.Model):
    #Nome completo da vitima
    full_name = models.CharField(max_length=150, blank=True, null=True)
    #Primeiro nome da vitima - campo criado para o Memorial
    first_name = models.CharField(max_length=100, blank=True, null=True)
    #Data da morte da vitima 
    date = models.DateField(help_text='Date of death')
    #Link para a noticia
    link = models.URLField(help_text='Link for news')
    #Alguma descricaoo adicional feita pela moderacao
    comment = models.TextField(help_text='Adicional description or comment', blank=True, null=True)
    #Flag para mostrar se o registro esta aprovado para publicacao
    approved = models.BooleanField()
    def __unicode__(self):
        return self.full_name
    
