from deaths.models import Death
from django.contrib import admin

class DeathAdmin(admin.ModelAdmin):
    list_display = ('full_name','first_name','date', 'approved')
    list_editable = ('first_name', 'approved')
    list_filter = ['approved','first_name']
    search_fields = ['full_name']
    ordering = ['-date']
admin.site.register(Death, DeathAdmin)    
