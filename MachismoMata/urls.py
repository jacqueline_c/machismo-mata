from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$','deaths.views.index'),
    url(r'^envie-noticias/','deaths.views.send'),
    url(r'^sobre/$', 'deaths.views.about'),
    url(r'^vitimas/$', 'deaths.views.victims'),
    url(r'^depoimentos/$', 'testimonies.views.index'),
    url(r'^envie-depoimento/$', 'testimonies.views.send'),
    url(r'^admin/', include(admin.site.urls)),
)
