<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1334945790456" ID="ID_395152785" MODIFIED="1334945846977" TEXT="LOG PARA A CRIA&#xc7;&#xc3;O DE UM PROJETO">
<node CREATED="1334945883429" FOLDED="true" ID="ID_1370797855" MODIFIED="1334947472177" POSITION="right" TEXT="Criar um diret&#xf3;rio">
<node CREATED="1334946012331" ID="ID_1640267266" MODIFIED="1334946105528">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      jacqueline@OrangoToe:~$ mkdir MM
    </p>
    <p>
      jacqueline@OrangoToe:~$ cd MM/
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334946168125" FOLDED="true" ID="ID_597781515" MODIFIED="1334947472874" POSITION="right" TEXT="Criar um ambiente virtual">
<node CREATED="1334946180805" ID="ID_246460513" MODIFIED="1334946186104" TEXT="Virtualenv">
<node CREATED="1334946246037" ID="ID_723830135" MODIFIED="1334946247808" TEXT="jacqueline@OrangoToe:~$ virtualenv MM --no-site-packages"/>
<node CREATED="1334946284029" ID="ID_1159460834" MODIFIED="1334946286728" TEXT="Ativar ambiente virtual">
<node CREATED="1334946293917" ID="ID_634068702" MODIFIED="1334946294712" TEXT="jacqueline@OrangoToe:~/MM$ source bin/activate"/>
</node>
</node>
<node CREATED="1334946187789" ID="ID_214667407" MODIFIED="1334946198669" TEXT="VirtualenvWrapper"/>
</node>
<node CREATED="1334946313261" FOLDED="true" ID="ID_466808128" MODIFIED="1334947473721" POSITION="right" TEXT="Instalar o Django no ambiente virtual">
<node CREATED="1334946333013" ID="ID_1649869243" MODIFIED="1334946334509" TEXT="(MM)jacqueline@OrangoToe:~/MM$ easy_install django"/>
</node>
<node CREATED="1334946369837" FOLDED="true" ID="ID_965192408" MODIFIED="1334947474434" POSITION="right" TEXT="Criar um projeto Django">
<node CREATED="1334946379717" ID="ID_90668329" MODIFIED="1334946381199" TEXT="(MM)jacqueline@OrangoToe:~/MM$ django-admin.py startproject MachismoMata"/>
</node>
<node CREATED="1334946444597" FOLDED="true" ID="ID_1189923547" MODIFIED="1334947474913" POSITION="right" TEXT="Dar permiss&#xe3;o de execu&#xe7;&#xe3;o ao manage.py">
<node CREATED="1334946454461" ID="ID_8130876" MODIFIED="1334946455224" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata$ chmod +x manage.py"/>
</node>
<node CREATED="1334946464821" FOLDED="true" ID="ID_1452594336" MODIFIED="1334947476226" POSITION="right" TEXT="Criar as primeiras configura&#xe7;&#xf5;es do Git">
<node CREATED="1334946500053" ID="ID_1924804452" MODIFIED="1334946501617" TEXT="Iniciar reposit&#xf3;rio Git">
<node CREATED="1334946508093" ID="ID_1083575484" MODIFIED="1334946509386" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git init"/>
</node>
<node CREATED="1334946517021" ID="ID_996331682" MODIFIED="1334946521096" TEXT="Adicionar remote no Git">
<node CREATED="1334946529685" ID="ID_337287917" MODIFIED="1334946532088" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git remote add origin https://jacdejour@bitbucket.org/jacdejour/machismo-mata.git"/>
</node>
<node CREATED="1334946554605" ID="ID_1254226483" MODIFIED="1334946556342" TEXT="Adicionar arquivo .gitignore no diret&#xf3;rio do Git (MM, no caso)">
<node CREATED="1334946567661" ID="ID_304968073" MODIFIED="1334946580911">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Meu exemplo &#233;:
    </p>
    <p>
      &#160;&#160;1 *.pyc
    </p>
    <p>
      &#160;&#160;2 *~
    </p>
    <p>
      &#160;&#160;3 *.swp
    </p>
    <p>
      &#160;&#160;4 ?humbs.db
    </p>
    <p>
      &#160;&#160;5 *egg-info
    </p>
    <p>
      &#160;&#160;6 bin/
    </p>
    <p>
      &#160;&#160;7 include/
    </p>
    <p>
      &#160;&#160;8 lib/
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1334946618101" FOLDED="true" ID="ID_1332869862" MODIFIED="1334947477217" POSITION="right" TEXT="Criar uma aplica&#xe7;&#xe3;o dentro do projeto">
<node CREATED="1334946632013" ID="ID_553449721" MODIFIED="1334946633660" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata$ ./manage.py startapp deaths"/>
</node>
<node CREATED="1334946669509" FOLDED="true" ID="ID_483593753" MODIFIED="1334947477873" POSITION="right" TEXT="Logar e criar um banco de dados no MySQL">
<node CREATED="1334946679109" ID="ID_457410261" MODIFIED="1334946692460" TEXT="jacqueline@OrangoToe:~$ mysql -h localhost -u root -p"/>
<node CREATED="1334946699862" ID="ID_1958592690" MODIFIED="1334946701646" TEXT="mysql&gt; create database machismomata;"/>
</node>
<node CREATED="1334946878101" FOLDED="true" ID="ID_1578997053" MODIFIED="1334947478761" POSITION="right" TEXT="Alterar arquivo settings.py">
<node CREATED="1334946903005" ID="ID_308363852" MODIFIED="1334946904505" TEXT="Alterar no arquivo settings.py o nome(s) e email(s) do(s) administradore(s) para que, quando o sistema estiver em produ&#xe7;&#xe3;o (DEBUG = False), os erros sejam enviados para esses emails"/>
<node CREATED="1334946911997" ID="ID_938337801" MODIFIED="1334946927879">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;6 ADMINS = (
    </p>
    <p>
      &#160;&#160;7&#160;&#160;&#160;&#160;&#160;# ('Jacqueline Castagnaro', 'jacquelinebf@gmail.com'),
    </p>
    <p>
      &#160;&#160;8 )
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334946954109" FOLDED="true" ID="ID_1461177591" MODIFIED="1334947480050" POSITION="right" TEXT="Alterar no arquivo settings.py as informa&#xe7;&#xf5;es sobre o BD utilizado">
<node CREATED="1334946963357" ID="ID_1558515420" MODIFIED="1334946974665">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;12 DATABASES = {
    </p>
    <p>
      &#160;13&#160;&#160;&#160;&#160;&#160;'default': {
    </p>
    <p>
      &#160;14&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'ENGINE': 'django.db.backends.mysql',
    </p>
    <p>
      &#160;15&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'NAME': 'machismomata',
    </p>
    <p>
      &#160;16&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'USER': 'root',&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      &#160;17&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'PASSWORD': '',&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      &#160;18&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'HOST': '',&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      &#160;19&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'PORT': '',&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
    </p>
    <p>
      &#160;20&#160;&#160;&#160;&#160;&#160;}
    </p>
    <p>
      &#160;21 }
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334947013629" FOLDED="true" ID="ID_1988279938" MODIFIED="1334947481441" POSITION="right" TEXT="Acrescentar no arquivo settings.py em INSTALLED_APPS o nome da aplica&#xe7;&#xe3;o criada">
<node CREATED="1334947039797" ID="ID_1519907211" MODIFIED="1334947048211">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      111 INSTALLED_APPS = (
    </p>
    <p>
      112&#160;&#160;&#160;&#160;&#160;'django.contrib.auth',
    </p>
    <p>
      113&#160;&#160;&#160;&#160;&#160;'django.contrib.contenttypes',
    </p>
    <p>
      114&#160;&#160;&#160;&#160;&#160;'django.contrib.sessions',
    </p>
    <p>
      115&#160;&#160;&#160;&#160;&#160;'django.contrib.sites',
    </p>
    <p>
      116&#160;&#160;&#160;&#160;&#160;'django.contrib.messages',
    </p>
    <p>
      117&#160;&#160;&#160;&#160;&#160;'django.contrib.staticfiles',
    </p>
    <p>
      118&#160;&#160;&#160;&#160;&#160;'deaths',
    </p>
    <p>
      123 )
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334947072453" FOLDED="true" ID="ID_1346905240" MODIFIED="1334947483081" POSITION="right" TEXT="Instalar pacote mysql-python para rodar o MySQL global dentro do virtualenv">
<node CREATED="1334947103197" ID="ID_1425636464" MODIFIED="1334947106064" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata$ easy_install mysql-python "/>
</node>
<node CREATED="1334947279525" FOLDED="true" ID="ID_405105784" MODIFIED="1334947484577" POSITION="right" TEXT="Criar os models da aplica&#xe7;&#xe3;o">
<node CREATED="1334947297877" ID="ID_254440124" MODIFIED="1334947313453">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;1 from django.db import models
    </p>
    <p>
      &#160;&#160;2
    </p>
    <p>
      &#160;&#160;3 class Death(models.Model):
    </p>
    <p>
      &#160;&#160;5&#160;&#160;&#160;&#160;&#160;full_name = models.CharField(max_length=150, blank=True, null=True)
    </p>
    <p>
      &#160;&#160;7&#160;&#160;&#160;&#160;&#160;first_name = models.CharField(max_length=100, blank=True, null=True)
    </p>
    <p>
      &#160;&#160;9&#160;&#160;&#160;&#160;&#160;date = models.DateField()
    </p>
    <p>
      &#160;11&#160;&#160;&#160;&#160;&#160;link = models.URLField()
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334947331445" FOLDED="true" ID="ID_280238720" MODIFIED="1334947485545" POSITION="right" TEXT="Conferir o sql para cria&#xe7;&#xe3;o de tabela">
<node CREATED="1334947340037" ID="ID_1725382170" MODIFIED="1334947341746" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata$ ./manage.py sql deaths"/>
</node>
<node CREATED="1334947409125" FOLDED="true" ID="ID_159794280" MODIFIED="1334947486001" POSITION="right" TEXT="Sincronizar o banco de dados (criar tabelas que n&#xe3;o existiam)">
<node CREATED="1334947416573" ID="ID_901060332" MODIFIED="1334947417472" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata$ ./manage.py syncdb"/>
</node>
<node CREATED="1334947499405" FOLDED="true" ID="ID_1977672419" MODIFIED="1334947632698" POSITION="right" TEXT="Passar altera&#xe7;&#xf5;es para o reposit&#xf3;rio Git">
<node CREATED="1334947527685" ID="ID_1933048372" MODIFIED="1334947534849" TEXT="Conferir status do Git">
<node CREATED="1334947539991" ID="ID_520977730" MODIFIED="1334947541453" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git status"/>
</node>
<node CREATED="1334947547973" ID="ID_760046825" MODIFIED="1334947549564" TEXT="Adicionar arquivos na staging area do Git">
<node CREATED="1334947556181" ID="ID_926166132" MODIFIED="1334947557371" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git add MachismoMata/deaths/"/>
</node>
<node CREATED="1334947571629" ID="ID_686998212" MODIFIED="1334947573209" TEXT="Dar commit no Git">
<node CREATED="1334947591285" ID="ID_1190821415" MODIFIED="1334947593443" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git commit -am &quot;Created app deaths and its models.&quot;"/>
</node>
<node CREATED="1334947605869" ID="ID_60469639" MODIFIED="1334947607060" TEXT="Enviar para o reposit&#xf3;rio Git">
<node CREATED="1334947614165" ID="ID_462701999" MODIFIED="1334947615868" TEXT="(MM)jacqueline@OrangoToe:~/MM$ git push"/>
</node>
</node>
<node CREATED="1334947624573" FOLDED="true" ID="ID_1206153722" MODIFIED="1334948168505" POSITION="right" TEXT="Definir m&#xe9;todo __unicode__ nos models">
<node CREATED="1334947646338" ID="ID_1296567744" MODIFIED="1334948089095">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;13&#160;&#160;&#160;&#160;&#160;def __unicode__(self):
    </p>
    <p>
      &#160;14&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;return self.full_name
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334948099765" FOLDED="true" ID="ID_310453491" MODIFIED="1334948167594" POSITION="right" TEXT="Ativar o admin do site">
<node CREATED="1334948110621" ID="ID_1167658179" MODIFIED="1334948111225" TEXT="Descomentar o &quot;&apos;django.contrib.admin&apos;&quot; no INSTALLED_APPS;"/>
<node CREATED="1334948118549" ID="ID_554306774" MODIFIED="1334948120289" TEXT="Rodar o ./manage.py syncdb para atualizar as tabelas do banco de dados;"/>
<node CREATED="1334948128781" ID="ID_81648954" MODIFIED="1334948130833" TEXT="Descomentar as seguitnes tr&#xea;s linhas no urls.py:">
<node CREATED="1334948138405" ID="ID_1846260763" MODIFIED="1334948145239">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;4 from django.contrib import admin
    </p>
    <p>
      &#160;&#160;5 admin.autodiscover()
    </p>
    <p>
      &#160;16&#160;&#160;&#160;&#160;&#160;url(r'^admin/', include(admin.site.urls)),
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1334948163925" FOLDED="true" ID="ID_615738854" MODIFIED="1334948269010" POSITION="right" TEXT="Adicionar o app ao admin do projeto">
<node CREATED="1334948204941" ID="ID_1378732409" MODIFIED="1334948207473" TEXT="Criar um arquivo admin.py no diret&#xf3;rio do app;"/>
<node CREATED="1334948218093" ID="ID_503973256" MODIFIED="1334948219201" TEXT="Acrescentar as seguitnes linhas:">
<node CREATED="1334948229405" ID="ID_680958509" MODIFIED="1334948235304">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;1 from deaths.models import Death
    </p>
    <p>
      &#160;&#160;2 from django.contrib import admin
    </p>
    <p>
      &#160;&#160;3
    </p>
    <p>
      &#160;&#160;4 admin.site.register(Death)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334948252949" ID="ID_1617969696" MODIFIED="1334948265584" TEXT="Reiniciar o servidor;"/>
</node>
<node CREATED="1334948246557" FOLDED="true" ID="ID_847408653" MODIFIED="1334948488546" POSITION="right" TEXT="Personalize as op&#xe7;&#xf5;es do admin no arquivo admin.py">
<node CREATED="1334948282421" ID="ID_738207859" MODIFIED="1334948288482">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;4 class DeathAdmin(admin.ModelAdmin):
    </p>
    <p>
      &#160;&#160;5&#160;&#160;&#160;&#160;&#160;list_display = ('full_name','date', 'approved')
    </p>
    <p>
      &#160;&#160;6&#160;&#160;&#160;&#160;&#160;list_filter = ['approved','first_name']
    </p>
    <p>
      &#160;&#160;7&#160;&#160;&#160;&#160;&#160;search_fields = ['full_name']
    </p>
    <p>
      &#160;&#160;8 admin.site.register(Death, DeathAdmin)&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334948309245" FOLDED="true" ID="ID_582506477" MODIFIED="1334955313933" POSITION="right" TEXT="Personalizar o template do admin">
<node CREATED="1334948429669" ID="ID_182718665" MODIFIED="1334948431976" TEXT="Adicionar endere&#xe7;o de templates no TEMPLATE_DIRS no settings.py">
<node CREATED="1334948448389" ID="ID_798312625" MODIFIED="1334948458348">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      105 TEMPLATE_DIRS = (
    </p>
    <p>
      106&#160;&#160;&#160;&#160;&#160;#ATEN&#199;&#195;O!!! Precisamos trocar a linha abaixo de acordo com cada ambiente!&#160;&#160;&#160;
    </p>
    <p>
      107&#160;&#160;&#160;&#160;&#160;'/home/jacqueline/MM/MachismoMata/templates'
    </p>
    <p>
      111 )
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1334948468197" ID="ID_1668920175" MODIFIED="1334948469790" TEXT="Localizar onde est&#xe1; o django/contrib/admin/templates">
<node CREATED="1334948481901" ID="ID_970464117" MODIFIED="1334948483777" TEXT="(MM)jacqueline@OrangoToe:~/MM$ find $VIRTUALENVWRAPPER_HOOK_DIR  -iname &apos;admin&apos; | grep -i template "/>
</node>
<node CREATED="1334948513205" ID="ID_295144207" MODIFIED="1334948515089" TEXT="Copiar o html que deseja e passar para a nova pasta">
<node CREATED="1334948524093" ID="ID_133248601" MODIFIED="1334948526035" TEXT="(MM)jacqueline@OrangoToe:~/MM/MachismoMata/templates/admin$ cp /home/jacqueline/.virtualenvs/otcms/lib/python2.6/site-packages/django/contrib/admin/templates/admin/base_site.html base_site.html"/>
</node>
<node CREATED="1334948536917" ID="ID_1204751570" MODIFIED="1334948559305" TEXT="Alterar no .html copiado o que deseja"/>
</node>
<node CREATED="1334955135295" FOLDED="true" ID="ID_1299770293" MODIFIED="1334955312876" POSITION="right" TEXT="Criar as URLs do site no urls.py">
<node CREATED="1334955146375" ID="ID_382879590" MODIFIED="1334955194301">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;&#160;7 urlpatterns = patterns('',
    </p>
    <p>
      &#160;&#160;8&#160;&#160;&#160;&#160;&#160;url(r'^envie-noticias/','deaths.views.send' ),
    </p>
    <p>
      &#160;13 )&#160;
    </p>
  </body>
</html>
</richcontent>
</node>
<node CREATED="1334955207575" ID="ID_1405777899" MODIFIED="1334955311346" TEXT="Lembre-se que para voc&#xea; testar sua url, precisa criar uma fun&#xe7;&#xe3;o nas views (nem que seja um HelloWorld) para n&#xe3;o dar erros.">
<icon BUILTIN="idea"/>
</node>
</node>
</node>
</map>
